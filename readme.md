# .NET Core Example

## Prerequisites
* [.NET Core](https://www.microsoft.com/net/core) 

## Set up Project
* Create new Project: `dot net new`
* Resolve Dependencies: `dot net restore`
* Run Program: `dot net run`
